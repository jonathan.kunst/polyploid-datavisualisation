#' table UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList 
#' @importFrom rhandsontable rhandsontable rHandsontableOutput renderRHandsontable
mod_table_ui <- function(id){
  ns <- NS(id)
  tagList(
      DT::dataTableOutput(ns('table'))
      )
}
    
#' tab_table Server Functions
#'
#' @noRd 
mod_table_server <- function(id, rv, rvlab){
  moduleServer(id, function(input, output, session){
    ns <- session$ns
    
    if (rvlab == 'pqtlr_linkmap') {
      # render table of table list
     output$table <- DT::renderDataTable(rv[[rvlab]][[as.numeric(rv$select_map())]])
    } else {
      # render single table (always stored in list of tables)
      output$table <- DT::renderDataTable(rv[[rvlab]])
    }
  })
}
    
## To be copied in the UI
# mod_table_ui("tab_table_ui_1")
    
## To be copied in the server
# mod_table_server("tab_table_ui_1")
