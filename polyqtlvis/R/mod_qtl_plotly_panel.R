#' qtl_plotly_panel UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList 
#' @importFrom shinycssloaders withSpinner
#' @importFrom plotly ggplotly renderPlotly plotlyOutput highlight layout event_data
#' @importFrom ggplot2 ggplot aes geom_point geom_line geom_hline facet_wrap theme element_blank ylab
#' @importFrom dplyr bind_rows filter case_when
#' @importFrom magrittr %>%
mod_qtl_plotly_panel_ui <- function(id){
  ns <- NS(id)
  tagList(
    fluidRow(
      # models to include
      column(2, 
             # plot all models or a selection?
             checkboxInput(inputId = ns('check_model'),
                           label = 'All models',
                           value = TRUE),
             
             # selector for models appears when checkbox is unchecked
             conditionalPanel(condition = 'input.check_model == 0',
                              ns = ns,
                              selectizeInput(inputId = ns('choice_model'),
                                             label = 'Models',
                                             choices = 'Create a model first',
                                             multiple = TRUE)
                              )
             ),
    
      # linkage groups to include
      column(2,
             # plot all chromosomes or a selection?
             checkboxInput(inputId = ns('check_chrom'),
                           label = 'All chromosomes',
                           value = TRUE),
             
             # selector for chromosome appears when checkbox is unchecked
             conditionalPanel(condition = 'input.check_chrom == 0',
                                ns = ns,
                                selectizeInput(inputId = ns('choice_chrom'),
                                                label = 'Chromosomes',
                                                choices = 'Create a model first',
                                                multiple = TRUE)
                               )
             ),
      
      # Rescale or not
      column(1, checkboxInput(inputId = ns('check_rescale'),
                              label = 'Rescale',
                              value = FALSE)
             ),
      
      # update plot action button
      column(1, actionButton(inputId = ns('update_plot'),
                             label = 'Update plot')
             )
    ),
    
    withSpinner(plotlyOutput(outputId = ns('qtl_plotly'))),
    
  )
}
    
#' qtl_plotly_panel Server Functions
#'
#' @noRd 
mod_qtl_plotly_panel_server <- function(id, rv){
  moduleServer( id, function(input, output, session){
    ns <- session$ns
    
    # model selection for plotly object
    observeEvent(rv$models, {
      updateSelectInput(session, 'choice_model', 
                        choices = unique(rv$model_df$model))
    })
    
    # linkage groups selection for plotly object
    observeEvent(rv$models, {
      updateSelectInput(session, 'choice_chrom', 
                        choices = unique(rv$model_df$chromosome))
    })
    
    # generate plotly object ----
    observeEvent(input$update_plot, {
      
      if (input$check_model & input$check_chrom){
        # if all models and chromosome are needed
        rv$plot_df <- rv$model_df
      } else if (!input$check_model & input$check_chrom){
        # if specific models are needed
        rv$plot_df <- rv$plot_df %>%
          filter(model %in% input$choice_model)
      } else if (input$check_model & !input$check_chrom){
        # if specific chromosomes are needed
        rv$plot_df <- rv$plot_df <- rv$model_df %>%
          filter(chromosome %in% input$choice_chrom)
      } else if (!input$check_model & !input$check_chrom){
        # if both specific models and chromosomes are needed
        rv$plot_df <- rv$plot_df %>%
          filter(model %in% input$choice_model & chromosome %in% input$choice_chrom)
      }
      
      rescale <- isolate(input$check_rescale)
      
      # create ggplot object
      try(
        rv$qtl_ggplot <- ggplot(rv$plot_df, aes(x = cM, 
                                     y = if (rescale) {centered} else {stat}, 
                                     color = model)) +
          geom_point(aes(text = paste(
            paste('Model: ', model),
            paste(stattype, round(stat, 2), sep = ': '),
            paste('Threshold: ', round(threshold, 2)),
            paste('Marker: ', marker),
            sep = '\n'))) +
          geom_line() +
              geom_hline(aes(yintercept = if (rescale) {0} else {threshold}, color = model)) +
          facet_wrap(~chromosome) +
          ylab(if (input$check_rescale) {'Threshold standardised'} else {paste(unique(rv$plot_df$stattype), collapse = ' | ')}) +
          theme(
            panel.grid = element_blank(),
            panel.background = element_blank(),
            panel.border = element_blank(),
            strip.background = element_blank(),
            strip.placement = 'outside')
          )
    })
    
    # render of ggplot object as plotly object
    output$qtl_plotly <- renderPlotly({
      req(rv$qtl_ggplot)
      ggplotly(rv$qtl_ggplot, tooltip = 'text') %>% 
        highlight('plotly_selected') %>% 
        layout(title = list(text = 'Chromosome', xanchor = 'center'))
    })
  })
}
    
## To be copied in the UI
# mod_qtl_plotly_panel_ui("qtl_plotly_panel_ui_1")
    
## To be copied in the server
# mod_qtl_plotly_panel_server("qtl_plotly_panel_ui_1")
