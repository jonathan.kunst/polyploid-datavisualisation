#' The application User-Interface
#' 
#' @param request Internal parameter for `{shiny}`. 
#'     DO NOT REMOVE.
#' @import shiny
#' @noRd
app_ui <- function(request) {
  tagList(
    # Leave this function for adding external resources
    golem_add_external_resources(),
    
    # Your application UI logic 
    navbarPage(title = 'QTLvis', inverse = TRUE,
               
                # Input panel ----
                 tabPanel('Data import',
                          mod_import_panel_ui('import_panel_ui')
                        ),
               
               # IBD panel ----
               tabPanel('IBD',
                        mod_IBD_panel_ui("IBD_panel_ui")
                        ),
               
               # Model panel ----
               tabPanel('Model',
                        mod_model_panel_ui("model_panel_ui")
                        )
      )
    )
}

# External resources ----
#' Add external Resources to the Application
#' 
#' This function is internally used to add external 
#' resources inside the Shiny application. 
#' 
#' @import shiny
#' @importFrom golem add_resource_path activate_js favicon bundle_resources
#' @noRd
golem_add_external_resources <- function(){
  
  add_resource_path(
    'www', app_sys('app/www')
  )
 
  tags$head(
    favicon(),
    bundle_resources(
      path = app_sys('app/www'),
      app_title = 'polyqtlvis'
    )
    # Add here other external resources
    # for example, you can add shinyalert::useShinyalert() 
  )
}

